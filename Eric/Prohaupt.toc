\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}
\defcounter {refsection}{0}
\select@language {ngerman}
\contentsline {section}{\numberline {1}Einleitung}{1}{section.1}
\contentsline {section}{\numberline {2}Theorie}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}Polarisation}{1}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Fresnel'sche Formeln}{2}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Brewster'sches Gesetz}{3}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Doppelbrechung}{3}{subsection.2.4}
\contentsline {section}{\numberline {3}Aufbau und Durchf\IeC {\"u}hrung}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Versuchsaufbau}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Durchf\IeC {\"u}hrung}{5}{subsection.3.2}
\contentsline {section}{\numberline {4}Auswertung}{6}{section.4}
\contentsline {subsection}{\numberline {4.1}Drehung der Schwingungsebene}{6}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Umrechnung der Rohdaten und Fehlerintervalle}{6}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}Bestimmung des Brechungsindex des Glasprismas}{7}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.3}Vergleich der gemessenen Drehung mit dem Experiment}{9}{subsubsection.4.1.3}
\contentsline {subsection}{\numberline {4.2}Direkte Messung des Brewsterwinkels}{10}{subsection.4.2}
\contentsline {section}{\numberline {5}Diskussion}{11}{section.5}
\contentsline {subsection}{\numberline {5.1}Bestimmung des Brechungsindex aus der Drehung der Schwingungsebene}{11}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Vergleich der gemessenen Drehung $\gamma $ mit der Theoriekurve}{12}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Direkte Messung des Brewsterwinkels}{12}{subsection.5.3}
\contentsline {section}{\nonumberline Literatur}{14}{section*.9}
