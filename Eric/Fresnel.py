# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

import maabara as ma
import numpy as np
import scipy as sp
import sympy as sym
import matplotlib as mpl 
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import uncertainties as uc
from uncertainties import unumpy as unp
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
mpl.rcParams['text.usetex']=True
mpl.rcParams['text.latex.unicode']=True
mpl.rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})

Phi, delta = np.loadtxt('Phi.dat',unpack=True)
alpha=90-0.5*Phi
gamma=delta-242.5

gamma45=np.array([52.,47.4,43.8,40.7])
alpha45=np.array([52.5,55.,57.5,60.])

m,b,tex=ma.data.linear_fit(alpha45,gamma45,np.array([0.1,0.1,0.1,0.1]),'f')

plt.close('all')
plt.clf
plt.figure(1)
plt.errorbar(alpha,gamma,0.1,0.5,label=r'Drehung $\gamma$ gemessen durch den Analysator.')
x=np.linspace(50,60,100)
plt.plot(x,m.n*x+b.n,'r-',label=tex)
plt.ylabel(r'Drehung der Schwingungsebene $\gamma$ [$^\circ$]')
plt.xlabel(r'Einfallswinkel $\alpha$ [$^\circ$]')
plt.legend(loc=3)
plt.xticks(np.arange(40,95,5))
plt.xlim(42,93)
plt.grid()


plt.savefig("plot1.pdf",transparent=True,format="pdf",bbox_inches='tight')
 

# <codecell>

alpha

# <codecell>

gamma

# <codecell>


