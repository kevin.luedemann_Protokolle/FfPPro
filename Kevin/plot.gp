reset
set terminal epslatex color

n=1.51
f(x)=m*x+b
t(x)= (x>=atan(n)*180/pi) ? atan(-cos(x*pi/180-asin(sin(x*pi/180)/n))/cos(x*pi/180+asin(sin(x*pi/180)/n)))*180/pi-45 : atan(-cos(x*pi/180-asin(sin(x*pi/180)/n))/cos(x*pi/180+asin(sin(x*pi/180)/n)))*180/pi+135
e(x)= (x>=atan(1.51)*180/pi) ? atan(-cos(x*pi/180-asin(sin(x*pi/180)/1.51))/cos(x*pi/180+asin(sin(x*pi/180)/1.51)))*180/pi-45 : atan(-cos(x*pi/180-asin(sin(x*pi/180)/1.51))/cos(x*pi/180+asin(sin(x*pi/180)/1.51)))*180/pi+135
set fit logfile 'gamma.log'
set output 'winkel.tex'

set xlabel '$\alpha\;[\si{\degree}]$'
set ylabel '$\gamma\;[\si{\degree}]$'
set key bottom left

fit f(x) "polari.dat" u (90-$1*0.5):($2-242.5):4 via m,b
set fit logfile 'testtheo.log'
fit t(x) "polari.dat" u (90-$1*0.5):($2-242.5):4 via n
p f(x) t 'Regression', "polari.dat" u (90-$1*0.5):($2-242.5):3:4 w xye lt -1  t 'Messwerte', t(x) lt -1 lc 3 t 'Theoriefit', e(x) lt -1 t 'Theoriekurve'

set output
!epstopdf winkel.eps
