\documentclass[twoside,12pt,headinclude,bibliography=totocnumbered,parskip=half-]{scrartcl}

% Einstellungen für Schrift und Sprache
\usepackage[ngerman]{babel}      % Sprachpaket
\usepackage[utf8]{inputenc}      % Zeichenkodierung UTF8
\usepackage{microtype}           % Paket für mikrotypographische Verbesserungen
\usepackage[T1]{fontenc}         % Schriftartpaket
\usepackage{lmodern}             % Schriftartpaket
\usepackage{textcomp}            % Paket für Textsymbole

% Allgemeine Layout-Einstellungen
\usepackage{geometry}            % Benutzerdefiniertes Einstellen der Seitenränder
\geometry{left=35mm,right=35mm,top=38mm,bottom=38mm,bindingoffset=-0.5cm}
\setkomafont{captionlabel}{\sffamily\bfseries}
\setcapwidth[c]{.9\textwidth}

% Einstellungen für Kopf- und Fußzeilen
\renewcommand{\sectionmark}[1]{\markboth{\thesection{}. #1}{}}
\renewcommand{\subsectionmark}[1]{\markright{\thesubsection{}. #1}}
\usepackage[headsepline,footsepline,plainfootsepline]{scrpage2}
\ohead[]{\scshape\leftmark}
\ihead[]{\rightmark}
\ofoot[\pagemark]{\pagemark}
\lofoot[\scshape Fresnelsche Formeln und Polarisation]{\scshape Fresnelsche Formeln und Polarisation}
\refoot[Joscha Knolle]{Joscha Knolle}
\pagestyle{scrheadings}
\setkomafont{pageheadfoot}{\normalfont}

% Einstellungen für die Matheumgebung
\usepackage{amssymb}             % Mathe-Paket
\usepackage{amsmath}             % Mathe-Paket
\usepackage{exscale}             % Paket zur Skalierung

% Weitere Pakete
\usepackage{color}               % Farb-Paket
\usepackage{enumitem}            % Listen-Paket
\usepackage{graphicx}            % Paket zur Graphik-Ausgabe
\usepackage[hyphens]{url}        % Paket für URLs
\usepackage{multirow}            % Paket zum vertikalen Verbinden von Zellen
\usepackage{rotating}            % Paket zum Drehen von Tabellen 
\usepackage{fixltx2e}            % Repariert einige Dinge bzgl. Float-Umgebungen

% Paket für Einheiten
\usepackage{siunitx}
\sisetup{% 
   load-configurations = abbreviations, 
   per-mode = symbol,            % Darstellung von Brüchen mit /
   output-decimal-marker = {,},  % Dezimal-Trennungs-Zeichen
   separate-uncertainty,         % Fehler separat angeben
   list-final-separator = { und },
   list-pair-separator = { und },% Abschlusswort für Listen
   range-phrase = { bis }        % Trennwort für Zahlbereiche
}

% Pakete für Graphik-Positionierung
\usepackage{wrapfig}             % Paket zum Umfließen des Textes um Graphiken
\usepackage{subfigure}           % Paket für mehrere Abbildungen nebeneinander
\usepackage{ragged2e}            % Paket zur Absatzausrichtung (für sidecap benötigt)
\usepackage{float}               % Paket für zusätzliche Option zur Grafikpositionierung

% Pakete für Verweise
\usepackage[hyphens]{url}        % Paket für URLs
\usepackage[hidelinks]{hyperref} % Paket für interne Verweise im PDF-Dokument

% Benutzerdefinierte Befehle
\newcommand{\chisq}{$\chi^2$}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%DOKUMENTBEGINN%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\begin{titlepage}
\centering
\textsc{\Large Grundpraktikum der Fakultät für Physik\\[1.5ex] Georg--August--Universität Göttingen}
\rule{\textwidth}{1pt}\\[0.5cm]
{\huge \bfseries
Versuch 20\\[1.5ex]
Fresnelsche Formeln und Polarisation}\\[0.5cm]
\rule{\textwidth}{1pt}
\vspace*{2.0cm}
\begin{Large}\begin{tabular}{ll}
	Praktikant: & Joscha Knolle\\
	E-Mail: & joscha@htilde.de\\
	Mitarbeiter & Ole Schumann\\
	Durchgeführt am: & 28.02.2013\\
	Abgabe: & 22.03.2013 \\
\end{tabular}\end{Large}
\vspace*{0.8cm}
\begin{Large}\fbox{\begin{minipage}[t][2.5cm][t]{6cm} 
   Testiert:
\end{minipage}}\end{Large}
\end{titlepage}

\thispagestyle{plain}
\setcounter{page}{1}
\tableofcontents
\newpage


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%EINLEITUNG%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Einleitung}

Der französische Physiker \textsc{Étienne Malus} beobachtete 1808 durch einen Kalkspatkristall das Licht der Sonne, das sich in seinen Fenstern spiegelte. Dabei stellte er fest, dass sich die Intensität des Spiegelbildes ändert, wenn er den Kristall dreht. \cite[S. 543]{ger} Die mathematische Beschreibung dieses Phänomens, dass sich die Polarisationseigenschaften von Licht bei der Reflexion verändern, gelang \textsc{Augustin Fresnel} im Jahr 1822 mit den nach ihm benannten Formeln.

In diesem Versuch wollen wir also untersuchen, wie sich die Polarisation des Lichtes bei der Reflexion an einem Prisma verändert, und dabei insbesondere den \textsc{Brewster}-Winkel vermessen.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%THEORIE%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Theorie}

\subsection{Polarisation}

Licht ist eine elektromagnetische Welle, die sich, wie aus den \textsc{Maxwell}schen Gleichungen folgt, transversal ausbreitet. Als \emph{Polarisation} des Lichtes bezeichnet man die Schwingungsrichtung dieser Welle.

Ist die Richtung der Schwingung des elektrischen Feldes konstant, aber ändert sich die Amplitude periodisch, so spricht man von \emph{linearer Polarisation}. Umgekehrt kann auch die Amplitude konstant sein und die Schwingungsrichtung sich kontinuierlich mit konstanter Winkelgeschwindigkeit drehen. Hierbei spricht man von \emph{zirkularer Polarisation}. \cite[S. 325 ff.]{hec} Diese beiden Grundformen der Polarisation sind in Abbildung \ref{fig:polarisation} dargestellt.

\begin{figure}[!ht]
	\centering
	\subfigure[Lineare Polarisation\label{fig:polarisationlinear}]
		{\footnotesize\def\svgwidth{0.35\textwidth}\input{linear.pdf_tex}}
	\hspace*{0.1\textwidth}
	\subfigure[Zirkulare Polarisation\label{fig:polarisationzirkular}]
		{\footnotesize\def\svgwidth{0.35\textwidth}\input{zirkular.pdf_tex}}
	\caption{Schematische Darstellung von Polarisationszuständen \cite[bearbeitet vom Verfasser]{lp}}
	\label{fig:polarisation}
\end{figure}


\subsection[\textsc{Fresnel}sche Formeln]{Fresnelsche Formeln}
\label{subsec:fresnel}

Trifft Licht auf eine Grenzfläche zwischen zwei Materialien mit unterschiedlichen Brechungsindices, so wird ein Teil des Lichts gebrochen und ein anderer Teil des Lichts reflektiert. Dabei gilt das Reflexionsgesetz, dass Einfalls- und Ausfallswinkel der Reflexion gleich sind, sowie das \textsc{Snellius}sche Brechungsgesetz
\begin{align}\label{eq:brechung}
	n_1\sin\alpha=n_2\sin\beta
\end{align}
zwischen dem Einfallswinkel $\alpha$ und dem Ausfallswinkel des transmittierten Strahls $\beta$. \cite[S. 233]{dem}

Experimentell beobachtet man, dass das Verhältnis der Amplituden von reflektiertem und transmittiertem Licht von der Polarisation des einfallenden Lichts abhängt. Man unterteilt daher die drei auftretenden elektromagnetische Wellen in Komponenten senkrecht zur Einfallsebene und parallele Komponenten, die in der Einfallsebene liegen. Die genaue Bezeichnung der einzelnen Komponenten ist in Abbildung \ref{fig:fresnel} dargestellt.

\begin{figure}[!ht]
	\centering
	\def\svgwidth{0.6\textwidth}
	\small\input{fresnel.pdf_tex}
	\caption{Feldkomponenten bei Reflexion und Transmission an einer Grenzfläche}
	\label{fig:fresnel}
\end{figure}

Aus den \textsc{Maxwell}schen Gleichungen und der Bedingung, dass die parallele Komponente des elektrischen Feldes an der Grenzfläche stetig verläuft, erhält man für die reflektierte und gebrochene Welle die Amplitudenverhältnisse
\begin{align}
	\rho_s&=\frac{E_r^s}{E_i^s}=-\frac{\sin\left(\alpha-\beta\right)}{\sin\left(\alpha+\beta\right)}\text{,}\label{eq:rhos}\\
	\tau_s&=\frac{E_t^s}{E_i^s}=\frac{2\sin\beta\cos\alpha}{\sin\left(\alpha+\beta\right)}\text{,}\label{eq:taus}\\
	\rho_p&=\frac{E_r^p}{E_i^p}=\frac{\tan\left(\alpha-\beta\right)}{\tan\left(\alpha+\beta\right)}\text{,}\label{eq:rhop}\\
	\tau_p&=\frac{E_t^p}{E_i^p}=\frac{2\sin\beta\cos\alpha}{\sin\left(\alpha+\beta\right)\cos\left(\alpha-\beta\right)}\text{.}\label{eq:taup}
\end{align}
Diese Gleichungen werden \textsc{Fresnel}sche Formeln genannt; eine genaue Herleitung ist bei \cite[S. 233 f.]{dem} zu finden.


\subsection[\textsc{Brewster}-Winkel]{Brewster-Winkel}

Stehen die Wellenvektoren von reflektiertem und transmittiertem Strahl senkrecht aufeinander, gilt also $\alpha+\beta=\SI{90}{\degree}$, so wird der Nenner von Gleichung \eqref{eq:rhop} unendlich. Das reflektierte Licht hat also keine parallele Komponente. Den Einfallswinkel $\alpha_B$, für den dieses Verhalten auftritt, nennt man \textsc{Brewster}-Winkel.

Mit der Bedingung $\alpha_B+\beta=\SI{90}{\degree}$ und dem Brechungsgesetz \eqref{eq:brechung} erhält man nach \cite[S. 237]{dem} für den \textsc{Brewster}-Winkel die Formel
\begin{align}\label{eq:brewster}
	\tan\alpha_B=\frac{\sin\alpha_B}{\cos\alpha_B}=\frac{\sin\alpha_B}{\sin\left(\SI{90}{\degree}-\alpha_B\right)}=\frac{\sin\alpha_B}{\sin\beta}=\frac{n_2}{n_1}\text{.}
\end{align}


\subsection{Doppelbrechung}
\label{subsec:doppelbrechung}

Das \textsc{Snellius}sche Brechungsgesetz gilt nur in Medien, die \emph{optisch isotrop} sind. Dies sind Gase, die meisten Flüssigkeiten und Kristalle mit hoher Symmetrie. Betrachtet man hingegen Kristalle, die \emph{optisch anisotrop} sind, so hängt der Brechungsindex davon ab, in welcher Richtung das Licht in den Kristall fällt und welche Polarisation es besitzt.

Hat ein Kristall eine optische Achse mit hoher Symmetrie, so gilt für das Licht entlang dieser Achse ein Brechungsindex $n_o$. Als ordentliches Licht bezeichnet man Licht, das senkrecht zur optischen Achse verläuft und ebenfalls senkrecht zur optischen Achse polarisiert ist. Für ordentliches Licht gilt ebenfalls der Brechungsindex $n_o$. Dagegen gilt für das außerordentliche Licht, das senkrecht zur optischen Achse verläuft und in Richtung der optischen Achse polarisiert ist, ein anderer Brechungsindex $n_{ao}$. \cite[S. 544 f.]{ger}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%MATERIALIEN & METHODEN%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Materialien \& Methoden}
\label{sec:materialienundmethoden}

\subsection{Polarisatoren}
\label{subsec:polarisator}

Optische Elemente, die aus unpolarisiertem Licht polarisiertes Licht machen, nennt man \emph{Polarisatoren}. Neben der Eigenschaft, Licht zu polarisieren, können sie auch als \emph{Analysatoren} verwendet werden, um festzustellen, wie einfallendes Licht polarisiert ist. \cite[S. 544]{ger}

Man kann den Effekt der Doppelbrechung nach Abschnitt \ref{subsec:doppelbrechung} nutzen, um einen Polarisator zu konstruieren. Dazu wird ein Kalkspat-Einkristall diagonal zersägt und an der Schnittkante wieder zusammengekittet. Zwei Seiten werden eingeschwärzt. Der Strahlengang in einem solchen \textsc{Nicol}-Prisma ist in Abbildung \ref{fig:nicol} schematisch dargestellt.

\begin{figure}[!ht]
	\centering
	\def\svgwidth{0.5\textwidth}
	\small\input{nicol.pdf_tex}
	\caption{Schematische Darstellung des Strahlengangs für den ordentlichen Strahl (gepunktet) und den außerordentlichen Strahl (durchgezogen) im \textsc{Nicol}-Prisma \cite[S. 547, bearbeitet vom Verfasser]{ger}}
	\label{fig:nicol}
\end{figure}

Der ordentliche Strahl wird stärker gebrochen, sodass er in einem großen Winkel auf die Schnittfläche trifft (Punkt B), total reflektiert und an dem geschwärzten Rand absorbiert wird. Dagegen wird der außerordentliche Strahl durch eine geeignete Wahl des Brechungsindex' des Kittmaterials nur leicht gebrochen und tritt parallel versetzt wieder aus dem Prisma aus. Der austretende Strahl ist also linear polarisiert. \cite[S. 547]{ger}

Eine andere Möglichkeit, einen Polarisator herzustellen, ist die Verwendung dichroitischer Kristalle, die eine Komponente des elektrischen Feldstärkevektors deutlich stärker absorbieren als die anderen. Bei solchen Kristallen verschwindet schon nach einer geringen Schichtdicke die eine Polarisationsrichtung, es entsteht also linear polarisiertes Licht. Indem solche Kristalle gleichmäßig in einer dünnen, durchsichtigen Folie eingebettet werden, können großflächige Polarisationsfolien hergestellt werden. \cite[S. 212 f.]{wal}


\subsection{Versuchsaufbau}
\label{subsec:versuchsaufbau}

Für diesen Versuch steht eine optische Schiene mit einem Schwenkarm zur Verfügung. Auf dem festen Arm ist eine Quecksilberdampflampe montiert, zur Herstellung von monochromatischem Licht stehen außerdem zwei Linsen, ein Spalt und ein Grünfilter bereit. Auf dem Schwenkarm ist ein Okular angebracht und es steht eine weitere Linse zur Verfügung.

Daneben stehen zur Anbringung auf der Schiene ein Folienpolarisator und ein \textsc{Nicol}-Prisma als Analysator zur Verfügung, die beide in ihrer Polarisationsrichtung verdrehbar und mit einer Winkelskala ausgestattet sind. Auf dem Drehgelenk ist außerdem ein Drehteller angebracht, auf den ein zweites \textsc{Nicol}-Prisma oder ein Glasprisma gestellt werden können. In Abbildung \ref{fig:aufbau} ist der Versuchsaufbau schematisch dargestellt.

\begin{figure}[!ht]
	\centering
	\def\svgwidth{\textwidth}
	\footnotesize\input{aufbau.pdf_tex}
	\caption{Schematische Darstellung des Versuchsaufbaus}
	\label{fig:aufbau}
\end{figure}

Der Schwenkarm ist mit einer Winkelskala versehen, wobei ein Schwenkwinkel von $\varphi=\SI{0}{\degree}$ bedeutet, dass die optische Achse und der Schwenkarm eine Gerade bilden. In Abbildung \ref{fig:skizze} ist die Beziehung zwischen dem Schwenkwinkel $\varphi$ und dem Einfallswinkel $\alpha$ skizziert. Man erhält
\begin{align}\label{eq:umrechnung}
	\alpha=\SI{90}{\degree}-\varepsilon=\SI{90}{\degree}-\frac{1}{2}\varphi\text{.}
\end{align}

\begin{figure}[!ht]
	\centering
	\def\svgwidth{0.35\textwidth}
	\small\input{skizze.pdf_tex}
	\caption{Winkelbeziehungen am Versuchsaufbau}
	\label{fig:skizze}
\end{figure}

Nach \cite{lp} ist der Brechungsindex des verwendeten Glasprismas
\begin{align}\label{eq:n}
	n=\num{1,510 \pm 0005}\text{.}
\end{align}


\subsection{Drehung der Schwingungsebene}

Ist der Versuch wie in Abbildung \ref{fig:aufbau} aufgebaut und der Polarisator so eingestellt, dass \SI{45}{\degree}-polarisiertes Licht auf das Glasprisma trifft, so wird durch die Reflexion die Schwingungsebene des Lichtes gedreht. Um einen Zusammenhang zwischen dem Drehwinkel $\gamma$ der Schwingungsebene und dem Einfallswinkel $\alpha$ zu erhalten, ist in Abbildung \ref{fig:drehwinkel} der reflektierte Strahl in Komponenten senkrechter und paralleler Polarisation aufgeteilt.

\begin{figure}[!ht]
	\centering
	\def\svgwidth{0.3\textwidth}
	\input{drehwinkel.pdf_tex}
	\caption{Zerlegung des reflektierten Strahls}
	\label{fig:drehwinkel}
\end{figure}

Der Winkel $\Delta$ der Schwingungsebene des reflektierten Strahls ergibt sich, indem zur Polarisation des einfallenden Lichtes die Drehung $\gamma$ addiert wird. Umgekehrt ist der Drehwinkel also durch
\begin{align}\label{eq:gamma}
	\gamma=\Delta-\SI{45}{\degree}
\end{align}
gegeben. Unter Anwendung der geometrischen Beziehung und Verwendung der \textsc{Fresnel}schen Formeln \eqref{eq:rhos} und \eqref{eq:rhop} gilt für den Winkel der Schwingungsebene
\begin{align}\nonumber
	\tan\Delta&=\frac{E_r^s}{E_r^p}=\frac{\rho_s}{\rho_p}
	=-\frac{\sin\left(\alpha-\beta\right)\tan\left(\alpha+\beta\right)}{\sin\left(\alpha+\beta\right)\tan\left(\alpha-\beta\right)}\\
	&=-\frac{\sin\left(\alpha-\beta\right)\sin\left(\alpha+\beta\right)\cos\left(\alpha-\beta\right)}{\sin\left(\alpha+\beta\right)\cos\left(\alpha+\beta\right)\sin\left(\alpha-\beta\right)}
	=-\frac{\cos\left(\alpha-\beta\right)}{\cos\left(\alpha+\beta\right)}\text{.}\label{eq:lang}
\end{align}
Mit dem Brechungsgesetz \eqref{eq:brechung}, $n_1\approx1$ für Luft und $n_2=n$ nach Gleichung \eqref{eq:n} für das Glasprisma erhält man schließlich durch Einsetzen in Gleichung \eqref{eq:gamma}
\begin{align}\label{eq:theorie}
	\gamma=\arctan\left[-\frac{\cos\left(\alpha-\arcsin\frac{\sin\alpha}{n}\right)}{\cos\left(\alpha+\arcsin\frac{\sin\alpha}{n}\right)}\right]-\SI{45}{\degree}\text{.}
\end{align}
Dabei muss beachtet werden, dass dieser Zusammenhang beim \textsc{Brewster}-Winkel einen Sprung um \SI{180}{\degree} macht, die Bildung der Umkehrfunktion in Gleichung \eqref{eq:lang} aber nur eindeutig bis auf Verschiebungen um \SI{180}{\degree} ist. Um für einen Winkelbereich, der den \textsc{Brewster}-Winkel einschließt, eine vernünftige Darstellung des theoretisch erwarteten Drehwinkels zu erhalten, können für Winkel kleiner als der \textsc{Brewster}-Winkel die errechneten Drehwinkel um \SI{180}{\degree} in den positiven Bereich verschoben werden.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%DURCHFÜHRUNG%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Durchführung}

\subsection{Justierung des Versuchsaufbaus}

Zunächst haben wir den Versuchsaufbau ohne Prisma, Polarisator und Analysator aufgebaut und den Strahlengang justiert. Dazu haben wir den Spalt in den Brennweiten der ersten beiden Linsen positioniert, sodass paralleles Licht entsteht. Außerdem haben wir die dritte Linse auf dem Schwenkarm so positioniert, dass das grüne Lichtbündel scharf auf dem Objektiv abgebildet wurde, also das Okular in der Brennweite der dritten Linse steht.

Anschließend haben wir auf den Drehteller ein \textsc{Nicol}-Prisma gestellt und den Polarisator vor dem Drehteller montiert. Der Polarisator wurde so gedreht, dass im Okular kein Bild mehr sichtbar war, die Polarisationsrichtung des Polarisators also parallel zur Einfallsebene ausgerichtet ist. Dann haben wir den Polarisator zusätzlich um \SI{45}{\degree} verdreht, sodass das durchtretende Licht gleiche Anteile parallel und senkrecht zur Einfallsebene hat.

Als dritten vorbereitenden Schritt haben wir das Glasprisma auf den Drehtisch gestellt und justiert. Dazu wurde die Position des Glasprismas auf dem Tisch solange variiert, bis bei gleicher Okulareinstellung sowohl unter einem Schwenkwinkel von \SI{90}{\degree} als auch bei einem Schwenkwinkel von \SI{45}{\degree} das Bild des Lichtbündels genau unter dem Fadenkreuz des Okulars lag.


\subsection{Messungen}

Für die Messungen wurde der mit einer Winkelskala ausgestattete Analysator vor die dritte Linse eingebaut.

Als erste Messreihe haben wir den Schwenkwinkel $\varphi$ von \SIrange{0}{90}{\degree} in \SI{5}{\degree}-Schritten verändert. Bei jedem Schritt haben wir die Polarisationsrichtung des Analysators solange gedreht, bis das Bild des Lichtbündels im Okular verschwand. Der sich ergebende Winkel am Analysator $\psi$ wurde notiert.

Für die zweite Messreihe haben wir den Polarisator wieder um \SI{45}{\degree} zurückgedreht, sodass seine Polarisationsrichtung parallel zur Einfallsebene steht. Dann haben wir sechsmal den Schwenkwinkel $\varphi$ vermessen, unter dem das Bild des Lichtbündels im Okular verschwindet.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%AUSWERTUNG%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Auswertung}

\subsection{Messung des Drehwinkels}

Im ersten Versuchsteil haben wir für verschiedene Einfallswinkel die Drehung der Schwingungsebene des Lichtes vermessen. Aus den aufgenommenen Schwenkwinkeln $\varphi$ berechnen wir den Einfallswinkel $\alpha$ nach Gleichung \eqref{eq:umrechnung}, wobei der Fehler nach dem Fehlerfortpflanzungsgesetz \eqref{app:fehlerfortpflanzung} durch
\begin{align}\label{eq:umrechnungfehler}
	\sigma_\alpha=\frac{\sigma_\varphi}{2}
\end{align}
gegeben ist.

Um aus den abgelesenen Analysatorwinkeln $\psi$ die Drehung der Schwingungsebene $\gamma$ zu erhalten, ziehen wir von allen abgelesenen Winkeln den Analysatorwinkel $\psi_0$ bei $\alpha=\SI{90}{\degree}$, also $\varphi=\SI{0}{\degree}$, ab. Da für alle abgelesenen Analysatorwinkel der gleiche Fehler angenommen wurde, ergibt sich der Fehler des Drehwinkels $\gamma=\psi-\psi_0$ nach dem Fehlerfortpflanzungsgesetz \eqref{app:fehlerfortpflanzung} zu $\sigma_\gamma=\sqrt{2}\sigma_\psi$.

Die so erhaltenen Messwerte sind im Anhang in Tabelle \ref{tab:messwerte1} aufgeführt und in Abbildung \ref{fig:reflexion} aufgetragen. Zusätzlich sind dort die theoretischen Werte, die man nach Gleichung \eqref{eq:theorie} erwartet, aufgetragen.

\begin{figure}[!ht]
	\centering
	\input{reflexion.tex}
	\caption{Gemessener und theoretischer Verlauf der Drehung der Schwingungsebene des Lichtes}
	\label{fig:reflexion}
\end{figure}

Um aus dieser Messung den Brechungsindex des verwendeten Glasprismas zu berechnen, betrachten wir den Drehwinkel von \SI{45}{\degree}, bei dem das reflektierte Licht gerade senkrecht zur Einfallsebene polarisiert ist. Der Einfallswinkel, unter dem dieser Drehwinkel zustande kommt, ist also der \textsc{Brewster}-Winkel.

Da wir für keinen unserer gewählten Schwenkwinkel einen Drehwinkel von gerade \SI{45}{\degree} gemessen haben, fitten wir einen funktionalen Zusammenhang zwischen dem Einfallswinkel und dem Drehwinkel. Der theoretische Verlauf nach Gleichung \eqref{eq:theorie} gestattet es näherungsweise, eine lineare Abhängigkeit anzunehmen. Mittels \texttt{gnuplot} führen wir daher einen \chisq-Fit auf den Zusammenhang $\gamma=a\cdot\alpha+b$ durch. In Tabelle \ref{tab:chisqergebnis} sind die von \texttt{gnuplot} ermittelten Fitparameter aufgetragen, die Fitgerade ist ebenfalls in Abbildung \ref{fig:reflexion} aufgetragen.

\begin{table}[!ht]
	\centering
	\begin{tabular}{|c|c|}
		\hline
		$a$ & \num{-1,50 \pm 0,05} \\
		\hline
		$b$ & \SI{132,5 \pm 2,9}{\degree}\\
		\hline
	\end{tabular}
	\caption{Ergebnis des \chisq-Fits}
	\label{tab:chisqergebnis}
\end{table}

Für $\gamma=\SI{45}{\degree}$ erhalten wir also gerade
\begin{align}
	\alpha=\frac{\gamma-b}{a}=\SI{58,3 \pm 2,8}{\degree}\text{,}
\end{align}
wobei der Fehler nach dem Gesetz der Fehlerfortpflanzung \eqref{app:fehlerfortpflanzung} durch
\begin{align}
	\sigma_\alpha=\frac{1}{\left|a\right|}\sqrt{\alpha^2\sigma_a^2+\sigma_b^2}
\end{align}
gegeben ist. Mit Gleichung \eqref{eq:brewster} erhalten wir unter der Annahme von $n_1\approx1$ für Luft den Brechungsindex
\begin{align}
	n=\tan\alpha=\num{1,62 \pm 0,18}\text{.}
\end{align}
Nach dem Fehlerfortpflanzungsgesetz \eqref{app:fehlerfortpflanzung} ist der Fehler gegeben durch
\begin{align}\label{eq:nfehler}
	\sigma_n=\frac{\sigma_\alpha}{\cos^2\alpha}\text{.}
\end{align}
Dabei muss beachtet werden, dass $\sigma_\alpha$ hier im Bogenmaß anzugeben ist, um eine dimensionslose Größe als Fehler des Brechungsindex' zu erhalten.


\subsection[Messung des \textsc{Brewster}-Winkels]{Messung des Brewster-Winkels}

Bei der zweiten Messung haben wir mehrfach den \textsc{Brewster}-Winkel bestimmt. Aus dem Schwenkwinkel $\varphi$ berechnen wir den Einfallswinkel $\alpha$ wieder über Gleichung \eqref{eq:umrechnung} mit dem Fehler nach Gleichung \eqref{eq:umrechnungfehler}.

Nach Gleichung \eqref{eq:brewster} bestimmen wir unter der Annahme von $n_1\approx1$ für Luft zu jeder Einzelmessung den Brechungsindex zu $n=\tan\alpha$, wobei der Fehler in Gleichung \eqref{eq:nfehler} angegeben ist. Die so erhaltenen Werte sind im Anhang in Tabelle \ref{tab:messwerte2} aufgeführt.

Die Messung des \textsc{Brewster}-Winkels wurde je zweimal von allen drei Praktikanten durchgeführt und beinhaltete jedes Mal die Abschätzung einer Mitte eines Winkelbereichs, in dem das Bild verschwand. Deswegen ist jede Messung mit einem eigenen Fehler versehen. Um aus diesen unterschiedlich stark fehlerbehafteten Messergebnissen ein Gesamtergebnis zu bilden, wird nach Gleichung \eqref{app:gewichtetesmittel} mit dem Fehler aus Gleichung \eqref{app:gewichteterfehler} das gewichtete Mittel der Messergebnisse zu
\begin{align}
	\overline{n}=\num{1,523 \pm 0,008}
\end{align}
bestimmt.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%DISKUSSION%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Diskussion}

\subsection{Einordnung der Ergebnisse}

Zur Einordnung der Ergebnisse sind in Tabelle \ref{tab:vergleich} die Messergebnisse für den Brechungsindex und ihre Abweichung vom Literaturwert aufgetragen.

\begin{table}[!ht]
	\centering
	\begin{tabular}{|c|c|c|}
		\hline
		Messmethode & Brechungsindex $n$ & Abweichung \\
		\hline
		Drehwinkel & \num{1,62 \pm 0,18} & \SI{7,28}{\percent} \\
		\textsc{Brewster}-Winkel & \num{1,523 \pm 0,008} & \SI{0,86}{\percent} \\
		\hline
		Literaturwert \cite{lp} & \num{1,51 \pm 0,005} & \\
		\hline
	\end{tabular}
	\caption{Vergleich der Ergebnisse}
	\label{tab:vergleich}
\end{table}

Aus der Messung des Drehwinkels erhalten wir eine relativ große Abweichung von etwa \SI{7,3}{\percent}, der Literaturwert liegt allerdings im Vertrauensbereich des Messergebnisses. Deutlich besser ist dagegen das Ergebnis aus der direkten Messung des \textsc{Brewster}-Winkels. Bei einem Fehler von weniger als \SI{1}{\percent} ist dieses Ergebnis sehr zufriedenstellend.


\subsection{Fehlerquellen}

Eine große Fehlerquelle liegt in der Justierung des Versuchsaufbaus. Zum Einen war der Polarisator nur mit einer groben Skala ausgestattet und ging schwerfällig, sodass das auf das Prisma fallende Licht nicht genau im Winkel \SI{45}{\degree} polarisiert war. Zum Anderen birgt auch die Positionierung des Prismas auf dem Drehtisch ein großes Fehlerpotential und konnte nicht vollkommen zufriedenstellend ausgeführt werden.

Bei beiden Messreihen musste durch Einstellung des Winkels des Analysators beziehungsweise des Schwenkarms der Winkel gefunden werden, unter dem das Bild im Okular verschwindet. Häufig konnte dazu aber kein genauer Punkt festgelegt, sondern nur durch Abschätzung die Mitte eines dunklen Bereichs ausgewählt werden. Diese Abschätzung ist natürlich auch mit einem Fehler verbunden.

Weiterhin ist es möglich, dass die Oberfläche des Glasprismas verschmutzt war, wodurch die Bildqualität vermindert worden sein konnte.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%ANHANG%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\pagenumbering{Roman}
\appendix
\addtocontents{toc}{\setcounter{tocdepth}{1}}
\setcounter{table}{0}
\setcounter{equation}{0}
\setcounter{figure}{0}
\renewcommand{\theequation}{\alph{equation}}
\renewcommand{\thetable}{\Alph{table}}
\renewcommand{\thefigure}{\Alph{figure}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%TABELLEN & ABBILDUNGEN%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Tabellen}

\begin{table}[H]
	\centering\small
	\begin{tabular}{|c|c|}
		\hline
		Einfallswinkel $\alpha$ & Drehwinkel $\gamma$ \\
		\hline
		\SI{90,00 \pm 0,05}{\degree} & \SI{0,00 \pm 0,15}{\degree} \\
		\SI{87,50 \pm 0,05}{\degree} & \SI{3,10 \pm 0,15}{\degree} \\
		\SI{85,00 \pm 0,05}{\degree} & \SI{5,90 \pm 0,15}{\degree} \\
		\SI{82,50 \pm 0,05}{\degree} & \SI{10,00 \pm 0,15}{\degree} \\
		\SI{80,00 \pm 0,05}{\degree} & \SI{11,10 \pm 0,15}{\degree} \\
		\SI{77,50 \pm 0,05}{\degree} & \SI{14,70 \pm 0,15}{\degree} \\
		\SI{75,00 \pm 0,05}{\degree} & \SI{18,20 \pm 0,15}{\degree} \\
		\SI{72,50 \pm 0,05}{\degree} & \SI{22,10 \pm 0,15}{\degree} \\
		\SI{70,00 \pm 0,05}{\degree} & \SI{31,10 \pm 0,15}{\degree} \\
		\SI{67,50 \pm 0,05}{\degree} & \SI{30,70 \pm 0,15}{\degree} \\
		\SI{65,00 \pm 0,05}{\degree} & \SI{36,70 \pm 0,15}{\degree} \\
		\SI{62,50 \pm 0,05}{\degree} & \SI{37,30 \pm 0,15}{\degree} \\
		\SI{60,00 \pm 0,05}{\degree} & \SI{36,80 \pm 0,15}{\degree} \\
		\SI{57,50 \pm 0,05}{\degree} & \SI{49,10 \pm 0,15}{\degree} \\
		\SI{55,00 \pm 0,05}{\degree} & \SI{52,60 \pm 0,15}{\degree} \\
		\SI{52,50 \pm 0,05}{\degree} & \SI{54,00 \pm 0,15}{\degree} \\
		\SI{50,00 \pm 0,05}{\degree} & \SI{60,10 \pm 0,15}{\degree} \\
		\SI{47,50 \pm 0,05}{\degree} & \SI{63,70 \pm 0,15}{\degree} \\
		\SI{45,00 \pm 0,05}{\degree} & \SI{62,90 \pm 0,15}{\degree} \\
		\hline	
	\end{tabular}
	\caption{Messwerte für die Messung des Drehwinkels}
	\label{tab:messwerte1}
\end{table}

\begin{table}[H]
	\centering\small
	\begin{tabular}{|c|c|}
		\hline
		\textsc{Brewster}-Winkel $\alpha$ & Brechungsindex $n$ \\
		\hline
		\SI{57 \pm 1}{\degree} & \num{1,54 \pm 0,06} \\
		\SI{56,8 \pm 0,25}{\degree} & \num{1,528 \pm 0,015} \\
		\SI{56,65 \pm 0,25}{\degree} & \num{1,519 \pm 0,015} \\
		\SI{56,8 \pm 0,5}{\degree} & \num{1,53 \pm 0,03} \\
		\SI{56,1 \pm 0,5}{\degree} & \num{1,488 \pm 0,029} \\
		\SI{56,8 \pm 0,25}{\degree} & \num{1,528 \pm 0,015} \\
		\hline
		Gewichtetes Mittel & \num{1,523 \pm 0,008} \\
		\hline
	\end{tabular}
	\caption{Messwerte für die Messung des \textsc{Brewster}-Winkels}
	\label{tab:messwerte2}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%FORMELSAMMLUNG%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Formelsammlung}

\subsection{Gewichteter Mittelwert}

Hat man in verschiedenen Messungen $n$ voneinander unabhängige Werte $x_i$ mit unterschiedlichen Fehlern $\sigma_i$ aufgenommen, so möchte man bei der Berechnung des Mittelwertes die Einzelergebnisse mit einem kleinen Fehler stärker gewichten als stark fehlerbehaftete Einzelergebnisse. Man berechnet also den \emph{gewichteten Mittelwert} $\overline{x}$ einer Messgröße zu
\begin{align}\label{app:gewichtetesmittel}
	\overline{x}=\dfrac{\sum\limits_{i=1}^n\frac{x_i}{\sigma_i^2}}{\sum\limits_{i=1}^n\frac{1}{\sigma_i^2}}
\end{align}
mit dem Fehler
\begin{align}\label{app:gewichteterfehler}
	\sigma_{\overline{x}}=\sqrt{\dfrac{1}{\sum\limits_{i=1}^n\frac{1}{\sigma_i^2}}}\text{.}
\end{align}


\subsection{Fehlerfortpflanzung}

Betrachtet man eine Messgröße $y$, die sich über einen funktionalen Zusammenhang $y=y(x_1,\ldots,x_n)$ aus $n$ verschiedenen Messgrößen $x_i$ ergibt, so möchte man wissen, welcher Fehler für $y$ sich aus den Fehlern $\sigma_i$ der Messgrößen $x_i$ ergibt. Nach dem \emph{Gesetz der Fehlerfortpflanzung} berechnet man den Fehler durch
\begin{align}\label{app:fehlerfortpflanzung}
	\sigma_y=\sqrt{\sum_{i=1}^n\sigma_i^2\left(\frac{\partial y}{\partial x_i}\right)^2}\text{.}
\end{align}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%LITERATURVERZEICHNIS%%%%%%%%%%%%%%%%%%%%%%%%%%

\renewcommand{\refname}{Literaturverzeichnis}
\begin{thebibliography}{AAA}
	\bibitem[DEM]{dem} Wolfgang Demtröder. \textit{Experimentalphysik 2. Elektrizität und Optik.} Vierte Auflage. Berlin, 2006.
	\bibitem[GER]{ger} Dieter Meschede. \textit{Gerthsen Physik.} Dreiundzwanzigste Auflage. Berlin, 2006.
	\bibitem[HEC]{hec} Eugene Hecht. \textit{Optics.} Vierte Auflage. San Francisco, 2002.
	\bibitem[LP]{lp} Lehrportal Physik. \textit{Fresnelsche Formeln und Polarisation.} Online im Internet: \url{http://lp.uni-goettingen.de/get/text/4330}, abgerufen am 16.03.13, 17:58 Uhr.
	\bibitem[WAL]{wal} Wilhelm Walcher. \textit{Praktikum der Physik.} Achte Auflage. Wiesbaden, 2004.
\end{thebibliography}


%%%%%%%%%%%%%%%%%%%%%%%%%%%DOKUMENTENDE%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}